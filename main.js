(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");




var routes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div style=\"text-align:center\">\n\n  <app-topmenu>\n\n    \n  </app-topmenu>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.sass":
/*!************************************!*\
  !*** ./src/app/app.component.sass ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'globaleagletemp';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.sass */ "./src/app/app.component.sass")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _weather_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./weather.service */ "./src/app/weather.service.ts");
/* harmony import */ var _topmenu_topmenu_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./topmenu/topmenu.component */ "./src/app/topmenu/topmenu.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_google_charts__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! angular-google-charts */ "./node_modules/angular-google-charts/fesm5/angular-google-charts.js");
/* harmony import */ var _tempconvert_pipe__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./tempconvert.pipe */ "./src/app/tempconvert.pipe.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _mapview_mapview_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./mapview/mapview.component */ "./src/app/mapview/mapview.component.ts");

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _topmenu_topmenu_component__WEBPACK_IMPORTED_MODULE_8__["TopmenuComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_9__["HomeComponent"],
                _tempconvert_pipe__WEBPACK_IMPORTED_MODULE_12__["TempConvertPipe"],
                _mapview_mapview_component__WEBPACK_IMPORTED_MODULE_16__["MapviewComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatButtonModule"],
                angular_google_charts__WEBPACK_IMPORTED_MODULE_11__["GoogleChartsModule"].forRoot(),
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatOptionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatSlideToggleModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_15__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatDatepickerModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatOptionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatSlideToggleModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_15__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatDatepickerModule"]
            ],
            providers: [_weather_service__WEBPACK_IMPORTED_MODULE_7__["WeatherService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='home'>\n\n    \n\n    <mat-radio-group [(ngModel)]=\"defaults.display\">\n        <mat-radio-button value=\"c\">C</mat-radio-button> \n        <span style=\"width:10px;display:inline-block\">  </span>\n         <mat-radio-button value=\"f\">F</mat-radio-button>\n      </mat-radio-group>\n\n    <form [formGroup]=\"tempForm\">  \n        <label>\n          <b>Add a temperature:</b> <br>\n          <div style='position:relative; display:inline-block;'><input type=\"text\" class='forminput' matInput  formControlName=\"temp\"  #newtemp>\n            <div class='inputoverlay' [hidden]=\"defaults.display=='f'\"> &#176;C</div>\n            <div class='inputoverlay' [hidden]=\"defaults.display=='c'\"> &#176;F</div>\n        \n        </div>\n          <input type=\"button\" matButton value='Add' (click)=\"addTemp(newtemp)\" class='green btn' [disabled]=\"!tempForm.valid\" [hidden]=\"!tempForm.get('temp').valid && tempForm.get('temp').touched\">\n        </label><br>\n       <div style='font-size:0.8em;'>\n          <mat-hint>use like : {{ defaults.display =='c' ? ' -60 ... 60' : '-76 ... 140' }}</mat-hint>  <br>\n          <mat-error>  \n              <span *ngIf=\"!tempForm.get('temp').valid && tempForm.get('temp').touched\">Please enter a valid temperature in deg {{ defaults.display =='c' ? 'C (-60 ... 60)' : 'F (-76 ... 140)' }}</span>  \n          </mat-error> \n      </div> \n        </form>\n\n        \n    <form [formGroup]=\"cityForm\">  \n        <label>\n          <b>Add temperatures from a city:</b> <br>\n          <input type=\"text\" class='forminput' matInput  formControlName=\"city\"  #newcity>\n          <input type=\"button\" matButton value='Add' (click)=\"addCity(newcity)\" class='green btn' [disabled]=\"!cityForm.valid\" [hidden]=\"!cityForm.get('city').valid && cityForm.get('city').touched\">\n        </label><br>\n       <div style='font-size:0.8em;'>\n          <mat-hint>use like : Montreal, QC</mat-hint>  <br>\n          <mat-error>  \n              <span *ngIf=\"!cityForm.get('city').valid && cityForm.get('city').touched\">Please enter a valid city name</span>  \n          </mat-error> \n      </div> \n      \n      \n      \n       \n\n\n        </form>\n<div>\n        <input matInput disabled [ngModel]=\"someDate | date:'yyyy-MM-dd'\" >\n  <input matInput [hidden]='true' [(ngModel)]=\"someDate\"  \n[matDatepicker]=\"picker\">\n          <!--<input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\"> -->\n          <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n          <mat-datepicker #picker></mat-datepicker>\n</div>\n  <mat-card class='container'> \n  \n    <mat-card-header>\n      \n      <mat-card-title><h2></h2></mat-card-title>\n    </mat-card-header>\n    \n    <mat-card-content>\n\n        <div class='tablecontents'>\n            <mat-table [dataSource]=\"dataSource\">\n              <ng-container matColumnDef=\"temperature\">\n                <mat-header-cell *matHeaderCellDef> Temperature </mat-header-cell>\n                <mat-cell *matCellDef=\"let client\"> {{client.forecast.forecastday.day.avgtemp_c | tempConvert : this.defaults.display  }} </mat-cell>\n              </ng-container>\n              <ng-container matColumnDef=\"date\">\n                  <mat-header-cell *matHeaderCellDef> Date </mat-header-cell>\n                  <mat-cell *matCellDef=\"let client\"> {{client.forecast.forecastday.date | date : \"longDate\" }}  </mat-cell>\n                </ng-container>\n\n                <ng-container matColumnDef=\"location\">\n                    <mat-header-cell *matHeaderCellDef> Location  </mat-header-cell>\n                    <mat-cell *matCellDef=\"let client\"> <a href=\"#mapv\" (click)=\"setLoc(client.location.lon, client.location.lat)\"> {{client.location.name  }}, {{ client.location.region }}, {{ client.location.country }}</a> </mat-cell>\n                  </ng-container>\n              \n              <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n              <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n            </mat-table>\n          </div>\n\n          <div style='' class='impvals '>\n\n\n            Important values <br>\n\n            <div class='vbox'> <span class='vkey'>Average</span> <span class='vval'>{{ temps.average | number : '1.2-2' | tempConvert : this.defaults.display  }}</span> \n            </div>\n           \n            <div class='vbox'><span class='vkey'>Median</span> <span class='vval'>{{ temps.median | number : '1.2-2' | tempConvert : this.defaults.display  }}</span> \n          </div>\n\n            <div class='vbox'> <span class='vkey'>Max</span> <span class='vval'>{{ temps.max | number : '1.2-2' | tempConvert : this.defaults.display  }}</span> \n            </div>\n\n              <div class='vbox'> <span class='vkey'>Min</span> <span class='vval'>{{ temps.min | number : '1.2-2' | tempConvert : this.defaults.display  }}</span> \n              </div>\n            \n              <google-chart\n              [title]=\"chart.title\"\n              [type]=\"chart.type\"\n              [(data)]=\"chart.data\"\n              [columnNames]=\"chart.columnNames\"\n              [options]=\"chart.options\">\n            </google-chart>\n\n\n          </div>\n\n\n\n      </mat-card-content>\n\n      </mat-card>\n\n      <app-mapview [lat]=\"position.latitude\" [lng]=\"position.longitude\" id=\"mapv\"></app-mapview>\n\n\n</div>\n  \n"

/***/ }),

/***/ "./src/app/home/home.component.sass":
/*!******************************************!*\
  !*** ./src/app/home/home.component.sass ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".home {\n  position: relative;\n  width: 100%;\n  margin-top: 100px;\n  padding: 20px;\n  text-align: center;\n  color: black; }\n\n.inputoverlay {\n  position: absolute;\n  right: 0px;\n  top: 0px;\n  padding: 5px;\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 1.5rem;\n  background: #7db4ec;\n  color: white;\n  font-weight: 300;\n  height: 100%;\n  border-radius: 0px 5px 5px 0px; }\n\n.container {\n  width: 100%; }\n\n@media (min-width: 768px) {\n  .impvals {\n    text-align: left;\n    font-size: 1.4em;\n    padding: 10px;\n    position: relative;\n    display: inline-block;\n    width: 30%; }\n  .tablecontents {\n    position: relative;\n    display: inline-block;\n    width: 70%;\n    vertical-align: top;\n    padding: 6px; }\n  .map {\n    width: 80%;\n    margin-left: 10%;\n    border: solid thin rgba(0, 0, 0, 0.4); } }\n\n@media (max-width: 768px) {\n  .impvals {\n    text-align: left;\n    font-size: 1.4em;\n    padding: 10px;\n    position: relative;\n    display: block;\n    width: 100%; }\n  .tablecontents {\n    position: relative;\n    display: inline-block;\n    width: 100%;\n    vertical-align: top;\n    padding: 6px; }\n  .map {\n    width: 100%;\n    margin-left: 0%;\n    border: solid thin rgba(0, 0, 0, 0.4); } }\n\na {\n  text-decoration: underline;\n  cursor: pointer; }\n\n.vbox {\n  margin: 10px;\n  width: 100%;\n  position: relative; }\n\n.vkey {\n  border-radius: 5px 0px 0px 5px;\n  margin-right: 0px;\n  padding: 5px;\n  background: #7bcc65;\n  color: white;\n  width: 50%;\n  display: inline-block; }\n\n.vval {\n  border-radius: 0px 5px 5px 0px;\n  text-align: right;\n  padding: 5px;\n  color: black;\n  background: #dbf2c8;\n  width: 50%;\n  display: inline-block; }\n\n.mat-cell {\n  font-size: 1.2rem; }\n\n.mat-header-cell {\n  font-size: 1.0rem; }\n\n.mat-row:nth-child(odd) {\n  background-color: #f5f9fc;\n  color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2NyZW5vLnRlY2gvZ2xvYmFsZWFnbGV0ZW1wdjIvZ2xvYmFsZWFnbGV0ZW1wL3NyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksbUJBQWtCO0VBQ2xCLFlBQVc7RUFDWCxrQkFBaUI7RUFDakIsY0FBYTtFQUNiLG1CQUFrQjtFQUNsQixhQUFZLEVBQUc7O0FBRW5CO0VBQ0ksbUJBQWtCO0VBQ2xCLFdBQVU7RUFDVixTQUFRO0VBQ1IsYUFBWTtFQUNaLG1CQUFrQjtFQUNsQixvQkFBbUI7RUFDbkIsa0JBQWlCO0VBQ2pCLG9CQUFtQjtFQUNuQixhQUFZO0VBQ1osaUJBQWdCO0VBQ2hCLGFBQVk7RUFDWiwrQkFBOEIsRUFBRzs7QUFFckM7RUFDSSxZQUFXLEVBQUc7O0FBRWxCO0VBQ0k7SUFDSSxpQkFBZ0I7SUFDaEIsaUJBQWdCO0lBQ2hCLGNBQWE7SUFDYixtQkFBa0I7SUFDbEIsc0JBQXFCO0lBQ3JCLFdBQVUsRUFBRztFQUNqQjtJQUNJLG1CQUFrQjtJQUNsQixzQkFBcUI7SUFDckIsV0FBVTtJQUNWLG9CQUFtQjtJQUNuQixhQUFZLEVBQUc7RUFDbkI7SUFDSSxXQUFVO0lBQ1YsaUJBQWdCO0lBQ2hCLHNDQUE2QixFQUFRLEVBQUE7O0FBRTdDO0VBQ0k7SUFDSSxpQkFBZ0I7SUFDaEIsaUJBQWdCO0lBQ2hCLGNBQWE7SUFDYixtQkFBa0I7SUFDbEIsZUFBYztJQUNkLFlBQVcsRUFBRztFQUNsQjtJQUNJLG1CQUFrQjtJQUNsQixzQkFBcUI7SUFDckIsWUFBVztJQUNYLG9CQUFtQjtJQUNuQixhQUFZLEVBQUc7RUFDbkI7SUFDSSxZQUFXO0lBQ1gsZ0JBQWU7SUFDZixzQ0FBNkIsRUFBUSxFQUFBOztBQUU3QztFQUNJLDJCQUEwQjtFQUMxQixnQkFBZSxFQUFHOztBQUN0QjtFQUNJLGFBQVk7RUFDWixZQUFXO0VBQ1gsbUJBQWtCLEVBQUc7O0FBRXpCO0VBQ0ksK0JBQThCO0VBQzlCLGtCQUFpQjtFQUNqQixhQUFZO0VBQ1osb0JBQW1CO0VBQ25CLGFBQVk7RUFDWixXQUFVO0VBQ1Ysc0JBQXFCLEVBQUc7O0FBRTVCO0VBQ0ksK0JBQThCO0VBQzlCLGtCQUFpQjtFQUNqQixhQUFZO0VBQ1osYUFBWTtFQUNaLG9CQUFtQjtFQUNuQixXQUFVO0VBQ1Ysc0JBQXFCLEVBQUc7O0FBRTVCO0VBQ0ksa0JBQWlCLEVBQUc7O0FBRXhCO0VBQ0ksa0JBQWlCLEVBQUc7O0FBRXhCO0VBQ0UsMEJBQXlCO0VBQ3pCLGFBQVksRUFBRyIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2FzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLmhvbWUge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAxMDBweDtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogYmxhY2s7IH1cblxuLmlucHV0b3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwcHg7XG4gICAgdG9wOiAwcHg7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xuICAgIGJhY2tncm91bmQ6ICM3ZGI0ZWM7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDBweCA1cHggNXB4IDBweDsgfVxuXG4uY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTsgfVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgICAuaW1wdmFscyB7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMS40ZW07XG4gICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICB3aWR0aDogMzAlOyB9XG4gICAgLnRhYmxlY29udGVudHMge1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgd2lkdGg6IDcwJTtcbiAgICAgICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgICAgICAgcGFkZGluZzogNnB4OyB9XG4gICAgLm1hcCB7XG4gICAgICAgIHdpZHRoOiA4MCU7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMCU7XG4gICAgICAgIGJvcmRlcjogc29saWQgdGhpbiByZ2JhKGJsYWNrLCAuNCk7IH0gfVxuXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgICAuaW1wdmFscyB7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMS40ZW07XG4gICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLnRhYmxlY29udGVudHMge1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gICAgICAgIHBhZGRpbmc6IDZweDsgfVxuICAgIC5tYXAge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDAlO1xuICAgICAgICBib3JkZXI6IHNvbGlkIHRoaW4gcmdiYShibGFjaywgLjQpOyB9IH1cblxuYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgY3Vyc29yOiBwb2ludGVyOyB9XG4udmJveCB7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTsgfVxuXG4udmtleSB7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4IDBweCAwcHggNXB4O1xuICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBiYWNrZ3JvdW5kOiAjN2JjYzY1O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB3aWR0aDogNTAlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxuXG4udnZhbCB7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4IDVweCA1cHggMHB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgYmFja2dyb3VuZDogI2RiZjJjODtcbiAgICB3aWR0aDogNTAlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxuXG4ubWF0LWNlbGwge1xuICAgIGZvbnQtc2l6ZTogMS4ycmVtOyB9XG5cbi5tYXQtaGVhZGVyLWNlbGwge1xuICAgIGZvbnQtc2l6ZTogMS4wcmVtOyB9XG5cbi5tYXQtcm93Om50aC1jaGlsZChvZGQpIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjlmYztcbiAgY29sb3I6IGJsYWNrOyB9XG5cblxuIl19 */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent, WeatherDataSource */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WeatherDataSource", function() { return WeatherDataSource; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _weather_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../weather.service */ "./src/app/weather.service.ts");





var HomeComponent = /** @class */ (function () {
    function HomeComponent(weatherService, fb) {
        var _this = this;
        this.weatherService = weatherService;
        this.fb = fb;
        this.position = {
            latitude: 18.5204,
            longitude: 73.8567,
        };
        /*
        [title]="chart.title"
                    [type]="chart.type"
                    [data]="chart.data"
                    [columnNames]="chart.columnNames"
                    [options]="chart.options">
                    */
        this.chart = {
            title: "Temperatures",
            type: "BarChart",
            data: [
                ["0", 0],
            ],
            columnNames: ['Location', 'Temperature'],
            options: {},
            width: 550,
            height: 600,
        };
        this.dataSource = new WeatherDataSource(this.weatherService);
        this.displayedColumns = ['temperature', 'date', 'location'];
        this.myDate = new Date();
        this.temp = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('');
        this.ini = 1;
        this.defaults = {
            location: "Montreal",
            dates: ["2019-02-17"] //, "2019-02-18", "2019-02-19"]
            ,
            display: 'c',
        };
        this.someDate = new Date();
        /* this.tempForm = fb.group({
           'temp' : [0, [Validators.required, Validators.min(-60), Validators.max(60)]],
         });
     */
        this.tempForm = fb.group({
            temp: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^(\-)?[0-9]+$'),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].max(60),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].min(-60)
            ])),
        });
        this.cityForm = fb.group({
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('Montreal', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(90)
            ]))
        });
        //this.addval = 0;
        this.temps = {
            average: 0, median: 0, max: 0, min: 0
        };
        this.weatherService.weathers.subscribe(function (w) {
            _this.calculateVals(w);
        });
        this.weatherService.weather.subscribe(function (w) {
            console.log("Setting new location", w);
            if (_this.ini) {
                _this.ini = 0;
                _this.chart.data = [];
            }
            if (!w)
                return;
            if (!w.location)
                return;
            _this.position.longitude = w.location.lon;
            _this.position.latitude = w.location.lat;
            _this.chart.data.push([w.location.name, w.forecast.forecastday.day.avgtemp_c]);
            _this.chart.data = _this.chart.data.slice();
            console.log("Chart", _this.chart.data);
        });
    }
    HomeComponent.prototype.setLoc = function (lon, lat) {
        this.position = {
            latitude: +lat,
            longitude: +lon
        };
    };
    HomeComponent.prototype.calculateVals = function (_w) {
        var total = 0;
        var min = -999999;
        var max = 99999;
        var median = 0;
        var n = 0;
        console.log("weather", _w);
        var v = [];
        for (var w in _w) {
            if (n == 0) {
                min = _w[0].forecast.forecastday.day.avgtemp_c;
                max = _w[0].forecast.forecastday.day.avgtemp_c;
            }
            var tw = _w[w];
            console.log("weather", tw);
            var tm = tw.forecast.forecastday.day.avgtemp_c;
            total += tm;
            if (tm < min)
                min = tm;
            if (tm > max)
                max = tm;
            n++;
            v.push(tm);
        }
        this.temps.average = total / n;
        this.temps.min = min;
        this.temps.max = max;
        this.temps.median = this.median(v);
    };
    HomeComponent.prototype.median = function (values) {
        values.sort(function (a, b) {
            return a - b;
        });
        if (values.length === 0)
            return 0;
        var half = Math.floor(values.length / 2);
        if (values.length % 2)
            return values[half];
        else
            return (values[half - 1] + values[half]) / 2.0;
    };
    HomeComponent.prototype.ngOnInit = function () {
        this.weatherService.loadWeathers(this.defaults.location, this.defaults.dates);
    };
    HomeComponent.prototype.addTemp = function (addval) {
        console.log(addval.value);
        var v = addval.value;
        v = this.defaults.display == 'c' ? v : (5 / 9 * (v - 32)).toFixed(2);
        this.weatherService.addTemp(+v);
    };
    HomeComponent.prototype.addCity = function (addval) {
        this.weatherService.loadForeignWeathers(addval.value, this.someDate.toISOString().slice(0, 10));
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.sass */ "./src/app/home/home.component.sass")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_weather_service__WEBPACK_IMPORTED_MODULE_4__["WeatherService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], HomeComponent);
    return HomeComponent;
}());

var WeatherDataSource = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](WeatherDataSource, _super);
    function WeatherDataSource(weatherService) {
        var _this = _super.call(this) || this;
        _this.weatherService = weatherService;
        return _this;
    }
    WeatherDataSource.prototype.connect = function () {
        //this.weatherService.loadWeathers();
        return this.weatherService.weathers;
    };
    WeatherDataSource.prototype.disconnect = function () { };
    return WeatherDataSource;
}(_angular_cdk_collections__WEBPACK_IMPORTED_MODULE_3__["DataSource"]));



/***/ }),

/***/ "./src/app/mapview/mapview.component.html":
/*!************************************************!*\
  !*** ./src/app/mapview/mapview.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"mapContainer\" class=\"map\"></div>"

/***/ }),

/***/ "./src/app/mapview/mapview.component.sass":
/*!************************************************!*\
  !*** ./src/app/mapview/mapview.component.sass ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media (min-width: 768px) {\n  .map {\n    width: 80%;\n    margin-left: 10%;\n    border: solid thin rgba(0, 0, 0, 0.4); } }\n\n@media (max-width: 768px) {\n  .map {\n    width: 100%;\n    margin-left: 0%;\n    border: solid thin rgba(0, 0, 0, 0.4); } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2NyZW5vLnRlY2gvZ2xvYmFsZWFnbGV0ZW1wdjIvZ2xvYmFsZWFnbGV0ZW1wL3NyYy9hcHAvbWFwdmlldy9tYXB2aWV3LmNvbXBvbmVudC5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0k7SUFDSSxXQUFVO0lBQ1YsaUJBQWdCO0lBQ2hCLHNDQUE2QixFQUFRLEVBQUE7O0FBRTdDO0VBQ0k7SUFDSSxZQUFXO0lBQ1gsZ0JBQWU7SUFDZixzQ0FBNkIsRUFBUSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbWFwdmlldy9tYXB2aWV3LmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gICAgLm1hcCB7XG4gICAgICAgIHdpZHRoOiA4MCU7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMCU7XG4gICAgICAgIGJvcmRlcjogc29saWQgdGhpbiByZ2JhKGJsYWNrLCAuNCk7IH0gfVxuXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgICAubWFwIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwJTtcbiAgICAgICAgYm9yZGVyOiBzb2xpZCB0aGluIHJnYmEoYmxhY2ssIC40KTsgfSB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/mapview/mapview.component.ts":
/*!**********************************************!*\
  !*** ./src/app/mapview/mapview.component.ts ***!
  \**********************************************/
/*! exports provided: MapviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapviewComponent", function() { return MapviewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var MapviewComponent = /** @class */ (function () {
    function MapviewComponent() {
        this.zoom = 15;
        this.latitude = 18.5204;
        this.longitude = 73.8567;
        this._lat = 0;
        this._lng = 0;
    }
    Object.defineProperty(MapviewComponent.prototype, "lat", {
        get: function () {
            // transform value for display
            return this._lat;
        },
        set: function (lat) {
            this._lat = lat;
            this.CenterMap(this._lng, this._lat);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MapviewComponent.prototype, "lng", {
        get: function () {
            // transform value for display
            return this._lng;
        },
        set: function (lng) {
            this._lng = lng;
            this.CenterMap(this._lng, this._lat);
        },
        enumerable: true,
        configurable: true
    });
    MapviewComponent.prototype.ngOnInit = function () {
        this.map = new ol.Map({
            target: 'mapContainer',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([-73.631381, 45.419957]),
                zoom: 7
            })
        });
        console.log("MAP CREATED", this.map);
    };
    MapviewComponent.prototype.CenterMap = function (long, lat) {
        if (!this.map)
            return;
        console.log("Long: " + long + " Lat: " + lat);
        this.map.getView().setCenter(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
        this.map.getView().setZoom(10);
    };
    MapviewComponent.prototype.setCurrentPosition = function () {
        /* if ("geolocation" in navigator) {
           navigator.geolocation.getCurrentPosition(async (position) => {
             this.mapsWrapper.setCenter({ lat: position.coords.latitude, lng: position.coords.longitude })
             if (!this.userLocation) {
               this.userLocation = await this.mapsWrapper.createMarker(
                 {
                   position: { lat: position.coords.latitude, lng: position.coords.longitude },
                   clickable: false
                 })
             } else {
               this.userLocation.setPosition({ lat: position.coords.latitude, lng: position.coords.longitude })
             }
           })
         }*/
    };
    // Update User Location
    MapviewComponent.prototype.updateUserLocation = function () {
        /* navigator.geolocation.watchPosition(async (position) => {
           this.setCurrentPosition()
           this.checkUserLocation()
         })*/
    };
    // Check if user is inside work site
    MapviewComponent.prototype.checkUserLocation = function () {
        /*const bounds = this.siteLocation.getBounds()
          if (bounds.contains(this.userLocation.position)) {
            this.canCheckIn = true
          }*/
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('gmap'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MapviewComponent.prototype, "gmapElement", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Number])
    ], MapviewComponent.prototype, "lat", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Number])
    ], MapviewComponent.prototype, "lng", null);
    MapviewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mapview',
            template: __webpack_require__(/*! ./mapview.component.html */ "./src/app/mapview/mapview.component.html"),
            styles: [__webpack_require__(/*! ./mapview.component.sass */ "./src/app/mapview/mapview.component.sass")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MapviewComponent);
    return MapviewComponent;
}());



/***/ }),

/***/ "./src/app/tempconvert.pipe.ts":
/*!*************************************!*\
  !*** ./src/app/tempconvert.pipe.ts ***!
  \*************************************/
/*! exports provided: TempConvertPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TempConvertPipe", function() { return TempConvertPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TempConvertPipe = /** @class */ (function () {
    // The work of the pipe is handled in the tranform method with our pipe's class
    function TempConvertPipe() {
    }
    TempConvertPipe.prototype.transform = function (value, arg) {
        console.log(arg);
        arg = arg != null ? arg : 'c';
        if (value != null && !isNaN(value) && arg === 'f') {
            var temp = (value * (9 / 5) + 32);
            var places = 2;
            return temp.toFixed(places) + ' ' + String.fromCharCode(176) + 'F';
        }
        return value + ' ' + String.fromCharCode(176) + 'C';
        return;
    };
    TempConvertPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'tempConvert'
        })
        // The work of the pipe is handled in the tranform method with our pipe's class
    ], TempConvertPipe);
    return TempConvertPipe;
}());



/***/ }),

/***/ "./src/app/topmenu/topmenu.component.html":
/*!************************************************!*\
  !*** ./src/app/topmenu/topmenu.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='topmenu'>\n  <div class='menucontents'>\n  <img src='assets/img/footer_global_eagle_white.svg' class='logo'> <span class='title'> - Temperature </span>\n</div>\n</div>\n"

/***/ }),

/***/ "./src/app/topmenu/topmenu.component.sass":
/*!************************************************!*\
  !*** ./src/app/topmenu/topmenu.component.sass ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".topmenu {\n  background: #7ab7e6;\n  height: 50px;\n  position: fixed;\n  left: 0px;\n  top: 0px;\n  width: 100%;\n  z-index: 1001; }\n\n.menucontents {\n  width: 90%;\n  margin-left: 5%;\n  height: 100%;\n  padding: 2px;\n  top: 0px;\n  position: relative;\n  text-align: left;\n  margin-top: 5px;\n  color: white;\n  font-size: 20px; }\n\n.logo {\n  height: 40px;\n  display: inline-block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2NyZW5vLnRlY2gvZ2xvYmFsZWFnbGV0ZW1wdjIvZ2xvYmFsZWFnbGV0ZW1wL3NyYy9hcHAvdG9wbWVudS90b3BtZW51LmNvbXBvbmVudC5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksb0JBQW1CO0VBQ25CLGFBQVk7RUFDWixnQkFBZTtFQUNmLFVBQVM7RUFDVCxTQUFRO0VBQ1IsWUFBVztFQUNYLGNBQWEsRUFBRzs7QUFFcEI7RUFDSSxXQUFVO0VBQ1YsZ0JBQWU7RUFDZixhQUFZO0VBQ1osYUFBWTtFQUNaLFNBQVE7RUFDUixtQkFBa0I7RUFDbEIsaUJBQWdCO0VBQ2hCLGdCQUFlO0VBQ2YsYUFBWTtFQUNaLGdCQUFlLEVBQUc7O0FBR3RCO0VBQ0ksYUFBWTtFQUNaLHNCQUFxQixFQUFHIiwiZmlsZSI6InNyYy9hcHAvdG9wbWVudS90b3BtZW51LmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiXG4udG9wbWVudSB7XG4gICAgYmFja2dyb3VuZDogIzdhYjdlNjtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGxlZnQ6IDBweDtcbiAgICB0b3A6IDBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB6LWluZGV4OiAxMDAxOyB9XG5cbi5tZW51Y29udGVudHMge1xuICAgIHdpZHRoOiA5MCU7XG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBwYWRkaW5nOiAycHg7XG4gICAgdG9wOiAwcHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDIwcHg7IH1cblxuXG4ubG9nbyB7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxuXG5cbiJdfQ== */"

/***/ }),

/***/ "./src/app/topmenu/topmenu.component.ts":
/*!**********************************************!*\
  !*** ./src/app/topmenu/topmenu.component.ts ***!
  \**********************************************/
/*! exports provided: TopmenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopmenuComponent", function() { return TopmenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TopmenuComponent = /** @class */ (function () {
    function TopmenuComponent() {
    }
    TopmenuComponent.prototype.ngOnInit = function () {
    };
    TopmenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-topmenu',
            template: __webpack_require__(/*! ./topmenu.component.html */ "./src/app/topmenu/topmenu.component.html"),
            styles: [__webpack_require__(/*! ./topmenu.component.sass */ "./src/app/topmenu/topmenu.component.sass")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TopmenuComponent);
    return TopmenuComponent;
}());



/***/ }),

/***/ "./src/app/weather.service.ts":
/*!************************************!*\
  !*** ./src/app/weather.service.ts ***!
  \************************************/
/*! exports provided: WeatherService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WeatherService", function() { return WeatherService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");






var NO_WEATHER = {};
var WeatherService = /** @class */ (function () {
    function WeatherService(http) {
        this.http = http;
        /*
      http://history.openweathermap.org/data/2.5/history/city?q={city ID},{country code}&type=hour&start={start}&end={end}
        */
        // apiURL : string = 'http://api.openweathermap.org/data/2.5/weather?'q=London,uk&APPID=b66f032e63b1457eefe819eff3fc8777
        //http://api.apixu.com/v1/history.json?key=9a43b33fd3474dfe9f8192731192002&q=montreal&dt=2019-02-19
        this.apiKey = 'c6551dfb070f4982928211232192002';
        this.apiURL = "http://api.apixu.com/v1/history.json?key=" + this.apiKey;
        //{city ID},{country code}&type=hour&start={start}&end={end}
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        this._weather = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](NO_WEATHER);
        this.weather = this._weather.asObservable();
        //&q=&dt=2015-01-01
        this._weathers = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]([]);
        this.weathers = this._weathers.asObservable();
        this.givendates = ["2019-02-17", "2019-02-18", "2019-02-19"];
        this.dataStore = { weathers: [] };
    }
    WeatherService.prototype.addTemp = function (val) {
        var service = this;
        var newWeather = ({
            location: {
                name: "Montreal",
                region: "QC",
                country: "CANADA",
                lat: 0,
                lon: 0,
                tz_id: "",
                localtime_epoch: 0,
                localtime: ""
            },
            forecast: {
                forecastday: {
                    date: "2019-02-20",
                    date_epoch: 0,
                    day: {
                        maxtemp_c: 0,
                        maxtemp_f: 0,
                        mintemp_c: 0,
                        mintemp_f: 0,
                        avgtemp_c: val,
                        avgtemp_f: 0,
                    }
                }
            }
        });
        console.log(newWeather);
        service._weather.next(newWeather);
        service.dataStore.weathers.unshift(newWeather);
        service._weathers.next(Object.assign({}, service.dataStore).weathers);
    };
    WeatherService.prototype.loadForeignWeathers = function (city, d) {
        var service = this;
        service.getWeather(city, d).subscribe(function (w) {
            //service.dataStore.weathers.push(w);
            service._weathers.next(Object.assign({}, service.dataStore).weathers);
        });
    };
    WeatherService.prototype.loadWeathers = function (city, dates) {
        var service = this;
        for (var _i = 0, dates_1 = dates; _i < dates_1.length; _i++) {
            var d = dates_1[_i];
            service.getWeather(city, d).subscribe(function (w) {
                //service.dataStore.weathers.push(w); the push is automatically made in getWeather
                service._weathers.next(Object.assign({}, service.dataStore).weathers);
            });
        }
    };
    WeatherService.prototype.getWeather = function (city, date) {
        var service = this;
        return service.http.get(service.apiURL + "&q=" + city + "&dt=" + date).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (w) {
            var newWeather = ({
                location: {
                    name: w["location"]["name"],
                    region: w["location"]["region"],
                    country: w["location"]["country"],
                    lat: w["location"]["lat"],
                    lon: w["location"]["lon"],
                    tz_id: w["location"]["tz_id"],
                    localtime_epoch: w["location"]["localtime_epoch"],
                    localtime: w["location"]["localtime"]
                },
                forecast: {
                    forecastday: {
                        date: w["forecast"]["forecastday"][0]["date"],
                        date_epoch: w["forecast"]["forecastday"][0]["date_epoch"],
                        day: {
                            maxtemp_c: w["forecast"]["forecastday"][0]["day"]["maxtemp_c"],
                            maxtemp_f: w["forecast"]["forecastday"][0]["day"]["maxtemp_f"],
                            mintemp_c: w["forecast"]["forecastday"][0]["day"]["mintemp_c"],
                            mintemp_f: w["forecast"]["forecastday"][0]["day"]["mintemp_f"],
                            avgtemp_c: w["forecast"]["forecastday"][0]["day"]["avgtemp_c"],
                            avgtemp_f: w["forecast"]["forecastday"][0]["day"]["avgtemp_f"],
                        }
                    }
                }
            });
            console.log(newWeather);
            service._weather.next(newWeather);
            // this.w = [newWeather];
            service.dataStore.weathers.unshift(newWeather);
            return newWeather;
        }));
    };
    WeatherService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], WeatherService);
    return WeatherService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/creno.tech/globaleagletempv2/globaleagletemp/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map