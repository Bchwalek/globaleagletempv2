
import {Pipe, PipeTransform} from '@angular/core';
@Pipe({
    name: 'tempConvert'
  })
  // The work of the pipe is handled in the tranform method with our pipe's class
  export class TempConvertPipe implements PipeTransform {
    transform(value: number, arg) {
        console.log(arg);
        arg = arg != null? arg : 'c';
      if(value != null && !isNaN(value) && arg === 'f') {
        var temp = (value*(9/5) + 32);
        var places = 2;
        return temp.toFixed(places) + ' '+ String.fromCharCode(176) +'F';       
      }
  
      return value + ' ' + String.fromCharCode(176) +'C';
  
      return;
    }
  }