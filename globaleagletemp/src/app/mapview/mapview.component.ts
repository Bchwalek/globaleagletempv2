import { Component, OnInit, Input } from '@angular/core';

import { ViewChild } from '@angular/core';

declare var ol: any;
@Component({
  selector: 'app-mapview',
  templateUrl: './mapview.component.html',
  styleUrls: ['./mapview.component.sass']
})
export class MapviewComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
 
  userLocation: any;
  siteLocation: any;
  canCheckIn: boolean;
  zoom: number = 15;
  latitude: number = 18.5204;
  longitude: number = 73.8567;

  private _lat: number = 0;
  private _lng: number = 0;

  map: any;
  constructor() {

  
   }
  @Input() set lat(lat : number) {
    this._lat = lat;
    this.CenterMap(this._lng, this._lat);
  } 

  get lat(): number {
    // transform value for display
    return this._lat;
  }

  @Input() set lng(lng : number) {
    this._lng = lng;
    this.CenterMap(this._lng, this._lat);
  } 

  get lng(): number {
    // transform value for display
    return this._lng;
  }

  ngOnInit() {
    
    this.map = new ol.Map({
      target: 'mapContainer',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([-73.631381,45.419957]),
        zoom: 7
      })
    });
    console.log("MAP CREATED", this.map);

  }


  CenterMap(long, lat) {
    if(!this.map)return;
    console.log("Long: " + long + " Lat: " + lat);
  this.map.getView().setCenter(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
    this.map.getView().setZoom(10);
}

  setCurrentPosition(){
   /* if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(async (position) => {
        this.mapsWrapper.setCenter({ lat: position.coords.latitude, lng: position.coords.longitude })
        if (!this.userLocation) {
          this.userLocation = await this.mapsWrapper.createMarker(
            {
              position: { lat: position.coords.latitude, lng: position.coords.longitude },
              clickable: false
            })
        } else {
          this.userLocation.setPosition({ lat: position.coords.latitude, lng: position.coords.longitude })
        }
      })
    }*/
  }
  
  // Update User Location
  private updateUserLocation() {
   /* navigator.geolocation.watchPosition(async (position) => {
      this.setCurrentPosition()
      this.checkUserLocation()
    })*/
  }
  
  // Check if user is inside work site
  private checkUserLocation() {
  /*const bounds = this.siteLocation.getBounds()
    if (bounds.contains(this.userLocation.position)) {
      this.canCheckIn = true
    }*/
  }

}
