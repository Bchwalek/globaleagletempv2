import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherService } from './weather.service';
import { TopmenuComponent } from './topmenu/topmenu.component';
import { HomeComponent } from './home/home.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { GoogleChartsModule } from 'angular-google-charts';

import { TempConvertPipe } from './tempconvert.pipe';
 

import 'hammerjs'

import {      
  MatButtonModule,      
  MatMenuModule,      
  MatToolbarModule,      
  MatIconModule,      
  MatCardModule,      
  MatFormFieldModule,      
  MatInputModule,      
  MatRadioModule,      
  MatSelectModule,   
  MatSidenavModule,   
  MatOptionModule,      
  MatSlideToggleModule, 
  MatDatepickerModule,
  MatNativeDateModule,
} from '@angular/material';      

import {MatTableModule} from '@angular/material/table';
import { MapviewComponent } from './mapview/mapview.component';

@NgModule({
  declarations: [
    AppComponent,
    TopmenuComponent,
    HomeComponent,
    TempConvertPipe,
    MapviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientModule,
    MatNativeDateModule,
    FormsModule,      
    ReactiveFormsModule,      
    BrowserAnimationsModule,     
    MatButtonModule,      
    GoogleChartsModule.forRoot(),
  MatMenuModule,      
  MatToolbarModule,      
  MatIconModule,      
  MatCardModule,      
  MatFormFieldModule,      
  MatInputModule,      
  MatRadioModule,      
  MatSelectModule,   
  MatSidenavModule,   
  MatOptionModule,      
  MatSlideToggleModule,
  MatTableModule,
  MatDatepickerModule
    
  ],
  exports: [
    MatButtonModule,      
  MatMenuModule,      
  MatToolbarModule,      
  MatIconModule,   
  MatNativeDateModule,   
  MatCardModule,      
  MatFormFieldModule,      
  MatInputModule,      
  MatRadioModule,      
  MatSelectModule,   
  MatSidenavModule,   
  MatOptionModule,      
  MatSlideToggleModule,
  MatTableModule,
  MatDatepickerModule
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { 

}
