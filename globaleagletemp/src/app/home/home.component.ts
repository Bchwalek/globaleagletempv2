import { Component, OnInit, Input  } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import { Observable } from 'rxjs-compat/Observable';
import { DataSource } from '@angular/cdk/collections';
import { WeatherService, Weather } from '../weather.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { TempConvertPipe } from '../tempconvert.pipe';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent implements OnInit {



  defaults : any;

  position = {
    latitude:  18.5204,
    longitude: 73.8567,
  } 

  /*
  [title]="chart.title"
              [type]="chart.type"
              [data]="chart.data"
              [columnNames]="chart.columnNames"
              [options]="chart.options">
              */

              chart = {
                title : "Temperatures",
                type: "BarChart",
                data : [
                  ["0", 0],
               ],
               columnNames : ['Location', 'Temperature'],
               options : { },
               width : 550,
               height : 600,
              }
  
  tempForm : FormGroup;
  cityForm : FormGroup;
  temps : any;
  weathers : any;
  dataSource = new WeatherDataSource(this.weatherService);
  displayedColumns = ['temperature', 'date', 'location'];
  myDate = new Date();
  temp = new FormControl('');
  public someDate: Date;
  setLoc(lon, lat) {
    this.position = {
      latitude : +lat,
      longitude : +lon
    }
  }
  ini = 1;
  constructor(private weatherService : WeatherService, private fb: FormBuilder) {

    this.defaults = {
      location :  "Montreal",
      dates : ["2019-02-17"]//, "2019-02-18", "2019-02-19"]
      , display : 'c',
    };


   

    this.someDate = new Date();

   /* this.tempForm = fb.group({  
      'temp' : [0, [Validators.required, Validators.min(-60), Validators.max(60)]],  
    });
*/
    this.tempForm = fb.group({
      temp: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^(\-)?[0-9]+$'),
        Validators.max(60),
        Validators.min(-60)
      ])),
      
      // more form inputs
    })

    this.cityForm = fb.group({
      city: new FormControl('Montreal', Validators.compose([
        Validators.required,
        Validators.maxLength(90)
      ]))
    })

    //this.addval = 0;
    this.temps = {
      average: 0, median : 0, max: 0, min: 0
    }

    this.weatherService.weathers.subscribe((w) => {
      this.calculateVals(w);
    });

   

    this.weatherService.weather.subscribe((w) => {
      console.log("Setting new location", w);
      if(this.ini) {
        this.ini = 0;
        this.chart.data = [];
      }
      if(!w)return;
      if(!w.location)return;
      this.position.longitude = w.location.lon;
      this.position.latitude = w.location.lat;

      this.chart.data.push([w.location.name, w.forecast.forecastday.day.avgtemp_c]);
      this.chart.data = this.chart.data.slice();
      console.log("Chart", this.chart.data);
    })

  }

  calculateVals(_w : Weather[]) {
    var total = 0;
    var min = -999999;
    var max = 99999;
    var median = 0; var n =0;
    console.log("weather",_w);
    var v = [];
    



    for(let w in _w) {
      if(n==0) {
        min = _w[0].forecast.forecastday.day.avgtemp_c;
       max =  _w[0].forecast.forecastday.day.avgtemp_c;
      }
      var tw=_w[w];
      console.log("weather",tw);
      var tm = tw.forecast.forecastday.day.avgtemp_c;
        total+= tm;
        if(tm<min) min = tm;
        if(tm>max) max = tm;
        n++;
        v.push(tm);
    }

    this.temps.average = total/n;
    this.temps.min = min;
    this.temps.max = max;
    this.temps.median = this.median(v);


  }

  median(values : number[]) 
    {
     
      values.sort(function(a,b){
      return a-b;
    });
  
    if(values.length ===0) return 0;
  
    var half = Math.floor(values.length / 2);
  
    if (values.length % 2)
      return values[half];
    else
      return (values[half - 1] + values[half]) / 2.0;
  
  }


  ngOnInit() {
    this.weatherService.loadWeathers(this.defaults.location, this.defaults.dates);
  }

  addTemp(addval) {
    console.log(addval.value);
    let v = addval.value;
    v = this.defaults.display == 'c'? v : (5/9*(v - 32)).toFixed(2);
      this.weatherService.addTemp(+v)
  }

  addCity(addval) {
      this.weatherService.loadForeignWeathers(addval.value,  this.someDate.toISOString().slice(0, 10));
  }

}

export class WeatherDataSource extends DataSource<any> {
  constructor(private weatherService: WeatherService) {
    super();
  }
  connect(): Observable<Weather[]> {
    //this.weatherService.loadWeathers();
    return this.weatherService.weathers;
  }
  disconnect() {}
}
